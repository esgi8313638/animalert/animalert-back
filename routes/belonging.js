const express = require('express');
const router = express.Router();
const MBelonging = require('../models/Belonging');

router.get('/belongings/:userID', async (req, res) => {
  const { userID } = req.params;

  try {
    const result = await MBelonging.BelongingGetAllByUser(userID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de toutes les relations de propriété pour l\'utilisateur' });
  }
});

router.get('/belongings/:userID/:animalID', async (req, res) => {
  const { userID, animalID } = req.params;

  try {
    const result = await MBelonging.BelongingGet(userID, animalID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de la relation de propriété' });
  }
});

router.post('/belongings', async (req, res) => {
  const { userID, animalID, dateStatement } = req.body;

  try {
    const result = await MBelonging.BelongingCreate(userID, animalID, dateStatement);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la création de la relation de propriété' });
  }
});

router.put('/belongings/:userID/:animalID', async (req, res) => {
  const { userID, animalID } = req.params;
  const { dateStatement } = req.body;

  try {
    const result = await MBelonging.BelongingUpdate(userID, animalID, dateStatement);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de la relation de propriété' });
  }
});

router.delete('/belongings/:userID/:animalID', async (req, res) => {
  const { userID, animalID } = req.params;

  try {
    const result = await MBelonging.BelongingDelete(userID, animalID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de la relation de propriété' });
  }
});

module.exports = router;
