const express = require('express');
const router = express.Router();
const MAlerts = require('../models/Alerts');

router.get('/alerts', async (req, res) => {
  try {
    const alerts = await MAlerts.getAll();
    res.json(alerts);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de toutes les alertes' });
  }
});

router.get('/alerts/:alertID', async (req, res) => {
  const { alertID } = req.params;

  try {
    const alert = await MAlerts.getById(alertID);
    res.json(alert);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'alerte' });
  }
});

router.get('/alerts/:alertID/location', async (req, res) => {
  const { alertID } = req.params;

  try {
    const location = await MAlerts.getAlertLocation(alertID);
    res.json(location);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de la localisation de l\'alerte' });
  }
});

router.get('/alerts/:alertID/animal', async (req, res) => {
  const { alertID } = req.params;

  try {
    const animal = await MAlerts.getAlertAnimal(alertID);
    res.json(animal);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'animal associé à l\'alerte' });
  }
});

router.post('/alerts', async (req, res) => {
  const { userID, location, animalID, pictures } = req.body;

  try {
    const result = await MAlerts.AlertUserCreate(userID, location, animalID, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la création de l\'alerte' });
  }
});

router.put('/alerts/:alertID', async (req, res) => {
  const { alertID } = req.params;
  const { location, resolved, pictures } = req.body;

  try {
    const result = await MAlerts.AlertUserUpdate(alertID, location, resolved, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'alerte' });
  }
});

router.delete('/alerts/:alertID', async (req, res) => {
  const { alertID } = req.params;

  try {
    const result = await MAlerts.AlertUserDelete(alertID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'alerte' });
  }
});

module.exports = router;
