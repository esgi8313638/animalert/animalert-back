const express = require('express');
const router = express.Router();
const MPictures = require('../models/Pictures');

router.get('/pictures', async (req, res) => {
  try {
    const result = await MPictures.PictureGetAll();
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des images' });
  }
});

router.get('/pictures/:pictureID', async (req, res) => {
  const { pictureID } = req.params;

  try {
    const result = await MPictures.PictureGet(pictureID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'image' });
  }
});

router.post('/pictures', async (req, res) => {
  const { image, forUsers } = req.body;

  try {
    const result = await MPictures.PictureCreate(image, forUsers);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la création de l\'image' });
  }
});

router.put('/pictures/:pictureID', async (req, res) => {
  const { pictureID } = req.params;
  const { image, forUsers } = req.body;

  try {
    const result = await MPictures.PictureUpdate(pictureID, image, forUsers);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'image' });
  }
});

router.delete('/pictures/:pictureID', async (req, res) => {
  const { pictureID } = req.params;

  try {
    const result = await MPictures.PictureDelete(pictureID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'image' });
  }
});

module.exports = router;
