const express = require('express');
const router = express.Router();
const MAnimal = require('../models/Animals');

router.get('/animals', async (req, res) => {
  try {
    const animals = await MAnimal.AnimalsGetAll();
    res.json(animals);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de tous les animaux' });
  }
});

router.get('/animals/:animalID', async (req, res) => {
  const { animalID } = req.params;

  try {
    const animal = await MAnimal.AnimalGet(animalID);
    res.json(animal);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'animal' });
  }
});

router.post('/animals', async (req, res) => {
  const { name, breed, puceID, tattoo, color, fur, height, weight, description, pictures } = req.body;

  try {
    const result = await MAnimal.AnimalCreate(name, breed, puceID, tattoo, color, fur, height, weight, description, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la création de l\'animal' });
  }
});

router.put('/animals/:animalID', async (req, res) => {
  const { animalID } = req.params;
  const { name, breed, puceID, tattoo, color, fur, height, weight, description, pictures } = req.body;

  try {
    const result = await MAnimal.AnimalUpdate(animalID, name, breed, puceID, tattoo, color, fur, height, weight, description, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'animal' });
  }
});

router.delete('/animals/:animalID', async (req, res) => {
  const { animalID } = req.params;

  try {
    const result = await MAnimal.AnimalDelete(animalID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'animal' });
  }
});

module.exports = router;
