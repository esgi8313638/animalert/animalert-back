const express = require('express');
const router = express.Router();
const models = require('./models');

// Route pour obtenir tous les utilisateurs
router.get('/users', async (req, res) => {
  try {
    const users = await models.User.getAll();
    res.json(users);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des utilisateurs' });
  }
});

// Route pour obtenir un utilisateur par ID
router.get('/users/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const user = await models.User.getById(id);
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ error: 'Utilisateur non trouvé' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'utilisateur' });
  }
});

// Route pour obtenir un utilisateur par EMail
router.get('/users/:email', async (req, res) => {
  const { email } = req.params;

  try {
    const user = await models.User.getByEmail(email);
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ error: 'Utilisateur non trouvé' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'utilisateur' });
  }
});

// Route pour obtenir un utilisateur par EMail
router.get('/users/:phone', async (req, res) => {
  const { phone } = req.params;

  try {
    const user = await models.User.getByPhone(phone);
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ error: 'Utilisateur non trouvé' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'utilisateur' });
  }
});



// Route pour créer un nouvel utilisateur
router.post('/users', async (req, res) => {
  const user = req.body;

  try {
      const newUser = await models.User.createUser(user);
      res.json(newUser);
  } catch (error) {
      res.status(500).json({ error: 'Erreur lors de la création de l\'utilisateur' });
  }
});

// Route pour mettre à jour un utilisateur par ID
router.put('/users/:id', async (req, res) => {
  const { id } = req.params;
  const updatedFields = req.body;

  try {
      const updatedUser = await models.User.updateUser(id, updatedFields);
      res.json(updatedUser);
  } catch (error) {
      res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'utilisateur' });
  }
});

// Route pour supprimer un utilisateur par ID
router.delete('/users/:id', async (req, res) => {
  const { id } = req.params;

  try {
      const deletedUser = await models.User.deleteUser(id);
      res.json(deletedUser);
  } catch (error) {
      res.status(500).json({ error: 'Erreur lors de la suppression de l\'utilisateur' });
  }
});

router.post('/login', async (req, res) => {
  const { email, password } = req.body;

  try {
      const result = await models.User.authenticate(email, password);
      res.json(result);
  } catch (error) {
      res.status(401).json({ error: error.message });
  }
});

router.post('/users/checkToken', async (req, res) => {
  const { token } = req.body;

  try {
    const decodedToken = await models.User.verifyToken(token);
    const currentUser = await models.User.getById(decodedToken.userID);
    
    res.json(currentUser);
  } catch (error) {
    res.status(401).json({ error: 'Token invalide' });
  }
});

router.post('/logout', async (req, res) => {
  const { token } = req.body;

  try {
      await models.User.logout(token);
      res.json({ message: 'Déconnexion réussie' });
  } catch (error) {
      res.status(500).json({ error: 'Erreur lors de la déconnexion' });
  }
});

module.exports = router;
