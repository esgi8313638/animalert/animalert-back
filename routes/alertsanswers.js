const express = require('express');
const router = express.Router();
const MAlertsAnswers = require('../models/AlertsAnswers');

router.post('/alerts-answers', async (req, res) => {
  const { userID, content, title, linkedAnimalProof, pictures } = req.body;

  try {
    const result = await MAlertsAnswers.AlertsAnswersCreate(userID, content, title, linkedAnimalProof, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la création de la réponse' });
  }
});

router.get('/alerts-answers/:answerID', async (req, res) => {
  const { answerID } = req.params;

  try {
    const answer = await MAlertsAnswers.AlertsAnswersGet(answerID);
    res.json(answer);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de la réponse' });
  }
});

router.put('/alerts-answers/:answerID', async (req, res) => {
  const { answerID } = req.params;
  const { content, title, linkedAnimalProof, pictures } = req.body;

  try {
    const result = await MAlertsAnswers.AlertsAnswersUpdate(answerID, content, title, linkedAnimalProof, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de la réponse' });
  }
});

router.delete('/alerts-answers/:answerID', async (req, res) => {
  const { answerID } = req.params;

  try {
    const result = await MAlertsAnswers.AlertsAnswersDelete(answerID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de la réponse' });
  }
});

router.get('/alerts/:alertID/answers', async (req, res) => {
  const { alertID } = req.params;

  try {
    const answers = await MAlertsAnswers.AlertsAnswersGetByAlertID(alertID);
    res.json(answers);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des réponses liées à l\'alerte' });
  }
});

module.exports = router;
