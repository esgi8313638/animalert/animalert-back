const express = require('express');
const router = express.Router();
const MAlertsAnswersPictures = require('../models/AlertsAnswersPictures');

router.get('/alerts-answers-pictures/:answerID', async (req, res) => {
  const { answerID } = req.params;

  try {
    const pictures = await MAlertsAnswersPictures.AlertsAnswersPicturesGetByAnswerID(answerID);
    res.json(pictures);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des images liées à la réponse' });
  }
});

router.post('/alerts-answers-pictures', async (req, res) => {
  const { answerID } = req.params;
  const { pictures } = req.body;

  try {
    const result = await MAlertsAnswersPictures.AlertsAnswersPicturesCreate(answerID, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de l\'ajout d\'images à la réponse' });
  }
});

router.get('/alerts-answers-pictures/:answerID/:pictureID', async (req, res) => {
  const { answerID, pictureID } = req.params;

  try {
    const picture = await MAlertsAnswersPictures.AlertsAnswersPicturesGet(answerID, pictureID);
    res.json(picture);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'image liée à la réponse' });
  }
});

router.put('/alerts-answers-pictures/:answerID/:pictureID', async (req, res) => {
  const { answerID, pictureID } = req.params;
  const { image } = req.body;

  try {
    const result = await MAlertsAnswersPictures.AlertsAnswersPicturesUpdate(answerID, pictureID, image);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'image liée à la réponse' });
  }
});

router.delete('/alerts-answers-pictures/:answerID/:pictureID', async (req, res) => {
  const { answerID, pictureID } = req.params;

  try {
    const result = await MAlertsAnswersPictures.AlertsAnswersPicturesDelete(answerID, pictureID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'image liée à la réponse' });
  }
});

module.exports = router;
