const express = require('express');
const router = express.Router();
const MAlertsPictures = require('../models/AlertsPictures');

router.post('/alerts-pictures/:alertID', async (req, res) => {
  const { alertID } = req.params;
  const { pictures } = req.body;

  try {
    const result = await MAlertsPictures.AlertPictureAdd(alertID, pictures);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de l\'ajout d\'images à l\'alerte' });
  }
});

router.delete('/alerts-pictures/:alertID/:pictureID', async (req, res) => {
  const { alertID, pictureID } = req.params;

  try {
    const result = await MAlertsPictures.AlertPictureDelete(alertID, pictureID);
    res.json(result);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'image de l\'alerte' });
  }
});

module.exports = router;
