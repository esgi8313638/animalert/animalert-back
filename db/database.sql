CREATE TABLE Pictures (
    pictureID SERIAL PRIMARY KEY,
    image TEXT,
    forUsers BOOLEAN
);

CREATE TABLE Animals (
    animalID SERIAL PRIMARY KEY,
    name VARCHAR(255),
    breed VARCHAR(255),
    puceID VARCHAR(255),
    tattoo VARCHAR(255),
    color VARCHAR(255),
    fur VARCHAR(255),
    height DECIMAL,
    weight DECIMAL,
    description TEXT,
    pictures INTEGER REFERENCES Pictures(pictureID)
);

CREATE TABLE Users (
    userID SERIAL PRIMARY KEY,
    email VARCHAR(255),
    password VARCHAR(255),
    telNumber VARCHAR(255),
    name VARCHAR(255),
    surname VARCHAR(255),
    firstName VARCHAR(255),
    address VARCHAR(255),
    localization BOOLEAN,
    pictures INTEGER REFERENCES Pictures(pictureID)
);

CREATE TABLE Belonging (
    userID INTEGER REFERENCES Users(userID),
    animalID INTEGER REFERENCES Animals(animalID),
    dateStatement DATE,
    PRIMARY KEY (userID, animalID)
);


CREATE TABLE Alerts (
    alertID SERIAL PRIMARY KEY,
    userID INTEGER REFERENCES Users(userID),
    location VARCHAR(255),
    resolved BOOLEAN,
    animalID INTEGER REFERENCES Animals(animalID)
);

CREATE TABLE AlertsPictures (
    alertID INTEGER REFERENCES Alerts(alertID),
    pictureID INTEGER REFERENCES Pictures(pictureID),
    PRIMARY KEY (alertID, pictureID)
);

CREATE TABLE AlertsAnswers (
  AlertsAnswersID SERIAL PRIMARY KEY,
  userID INTEGER REFERENCES Users(userID),
  content VARCHAR(255),
  title VARCHAR(255),
  linkedAnimalProof INTEGER REFERENCES Animals(animalID)
);

CREATE TABLE AlertsAnswersPictures (
    AlertsAnswersID INTEGER REFERENCES AlertsAnswers(AlertsAnswersID),
    pictureID INTEGER REFERENCES Pictures(pictureID),
    PRIMARY KEY (AlertsAnswersID, pictureID)
);