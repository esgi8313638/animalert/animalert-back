var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var alertsRouter = require('./routes/alerts');
var alertsAnswersRouter = require('./routes/alertsanswers');
var alertsAnswersPicturesRouter = require('./routes/alertsanswerspictures');
var alertsPicturesRouter = require('./routes/alertspictures');
var animalsRouter = require('./routes/animals');
var belongingRouter = require('./routes/belonging');
var picturesRouter = require('./routes/pictures');
var usersRouter = require('./routes/users');

var app = express();
var port = process.env.PORT || 3000;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/alerts', alertsRouter);
app.use('/alerts-answers', alertsAnswersRouter);
app.use('/alerts-answers-pictures', alertsAnswersPicturesRouter);
app.use('/alerts-pictures', alertsPicturesRouter);
app.use('/animals', animalsRouter);
app.use('/belongings', belongingRouter)
app.use('/pictures', picturesRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Read used port from environment variable or use default port
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
