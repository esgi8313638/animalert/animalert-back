const pool = require('../db/db')

class AlertsPictures {
    static async AlertPictureAdd(alertid, pictures) {
		try {
			for (const picture of pictures) {
				const pictureInsertQuery = `INSERT INTO Pictures (image, forUsers) VALUES ('${picture}', false) RETURNING pictureid`;
				const pictureResult = await pool.query(pictureInsertQuery);
				const pictureid = pictureResult.rows[0].pictureid;
		
				const linkPictureToAlertQuery = `INSERT INTO AlertsPictures (alertid, pictureid) VALUES (${alertid}, ${pictureid})`;
				await pool.query(linkPictureToAlertQuery);
			}
		
			return { message: 'Images ajoutées avec succès à l\'alerte' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de l\'ajout d\'images à l\'alerte');
		}
	}

	static async AlertPictureDelete(alertid, pictureid) {
		try {
			const deletePictureLinkQuery = `DELETE FROM AlertsPictures WHERE alertid = ${alertid} AND pictureid = ${pictureid}`;
			await pool.query(deletePictureLinkQuery);
		
			const pictureUsageCountQuery = `SELECT COUNT(*) AS count FROM AlertsPictures WHERE pictureid = ${pictureid}`;
			const pictureUsageCountResult = await pool.query(pictureUsageCountQuery);
			const usageCount = pictureUsageCountResult.rows[0].count;
		
			if (usageCount === 0) {
				const deletePictureQuery = `DELETE FROM Pictures WHERE pictureid = ${pictureid}`;
				await pool.query(deletePictureQuery);
			}
		
			return { message: 'Image supprimée avec succès de l\'alerte' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la suppression de l\'image de l\'alerte');
		}
	}
}

module.exports = { AlertsPictures }