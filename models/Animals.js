const pool = require('../db/db')

class Animals {
	
	static async AnimalsGetAll() {
		try {
			const animalsQuery = `
				SELECT a.animalID, a.name, a.breed, a.puceID, a.tattoo, a.color, a.fur, a.height, a.weight, a.description,
					ap.pictureID, ap.image
				FROM Animals a
				LEFT JOIN AnimalsPictures ap ON a.animalID = ap.animalID`;
			
			const results = await pool.query(animalsQuery);
		
			const formattedAnimals = results.rows.map(row => ({
				animalID: row.animalID,
				name: row.name,
				breed: row.breed,
				puceID: row.puceID,
				tattoo: row.tattoo,
				color: row.color,
				fur: row.fur,
				height: row.height,
				weight: row.weight,
				description: row.description,
				pictures: results.rows.map(row => ({ pictureID: row.pictureID, image: row.image })),
			}));
		
			return formattedAnimals;
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la récupération de tous les animaux');
		}
	}
	  

	static async AnimalGet(animalID) {
		try {
			const animalQuery = `
				SELECT a.animalID, a.name, a.breed, a.puceID, a.tattoo, a.color, a.fur, a.height, a.weight, a.description,
					ap.pictureID, ap.image
				FROM Animals a
				LEFT JOIN AnimalsPictures ap ON a.animalID = ap.animalID
				WHERE a.animalID = ${animalID}`;
			
			const result = await pool.query(animalQuery);
		
			const animal = result.rows[0];
			if (!animal) {
				throw new Error('Animal non trouvé');
			}
		
			const formattedAnimal = {
				animalID: animal.animalID,
				name: animal.name,
				breed: animal.breed,
				puceID: animal.puceID,
				tattoo: animal.tattoo,
				color: animal.color,
				fur: animal.fur,
				height: animal.height,
				weight: animal.weight,
				description: animal.description,
				pictures: result.rows.map(row => ({ pictureID: row.pictureID, image: row.image })),
			};
		
			return formattedAnimal;
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la récupération de l\'animal');
		}
	}

	static async AnimalCreate(name, breed, puceID, tattoo, color, fur, height, weight, description, pictures) {
		try {
			const animalInsertQuery = `
				INSERT INTO Animals (name, breed, puceID, tattoo, color, fur, height, weight, description)
				VALUES ('${name}', '${breed}', '${puceID}', '${tattoo}', '${color}', '${fur}', ${height}, ${weight}, '${description}')
				RETURNING animalID`;
			
			const animalResult = await pool.query(animalInsertQuery);
			const animalID = animalResult.rows[0].animalID;
		
			for (const pictureID of pictures) {
				const linkPictureToAnimalQuery = `INSERT INTO AnimalsPictures (animalID, pictureID) VALUES (${animalID}, ${pictureID})`;
				await pool.query(linkPictureToAnimalQuery);
			}
		
			return { animalID, message: 'Animal ajouté avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la création de l\'animal');
		}
	}

	static async AnimalUpdate(animalID, name, breed, puceID, tattoo, color, fur, height, weight, description, pictures) {
		try {
			const animalUpdateQuery = `
				UPDATE Animals
				SET name = '${name}', breed = '${breed}', puceID = '${puceID}', tattoo = '${tattoo}',
					color = '${color}', fur = '${fur}', height = ${height}, weight = ${weight}, description = '${description}'
				WHERE animalID = ${animalID}`;
			
			await pool.query(animalUpdateQuery);
		
			const deletePicturesQuery = `DELETE FROM AnimalsPictures WHERE animalID = ${animalID}`;
			await pool.query(deletePicturesQuery);
		
			for (const pictureID of pictures) {
				const linkPictureToAnimalQuery = `INSERT INTO AnimalsPictures (animalID, pictureID) VALUES (${animalID}, ${pictureID})`;
				await pool.query(linkPictureToAnimalQuery);
			}
		
			return { message: 'Animal mis à jour avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la mise à jour de l\'animal');
		}
	}

	static async AnimalDelete(animalID) {
		try {
			const deletePicturesQuery = `DELETE FROM AnimalsPictures WHERE animalID = ${animalID}`;
			await pool.query(deletePicturesQuery);
		
			const deleteAnimalQuery = `DELETE FROM Animals WHERE animalID = ${animalID}`;
			await pool.query(deleteAnimalQuery);
		
			return { message: 'Animal supprimé avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la suppression de l\'animal');
		}
	}
	  
}

module.exports = { Animals };
  