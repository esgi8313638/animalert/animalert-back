const pool = require('../db/db')

class AlertsAnswersPictures {
    static async AlertsAnswersPicturesGetByAnswerID(answerID) {
        try {
            const picturesQuery = `
                SELECT pictureID, image
                FROM AlertsAnswersPictures
                WHERE AlertsAnswersID = ${answerID}`;
            
            const results = await pool.query(picturesQuery);
            
            const pictures = results.rows.map(row => ({
                pictureID: row.pictureID,
                image: row.image,
            }));
        
            return pictures;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération des images liées à la réponse');
        }
    }

    static async AlertsAnswersPicturesCreate(answerID, pictures) {
        try {
            for (const picture of pictures) {
                const pictureInsertQuery = `INSERT INTO Pictures (image, forUsers) VALUES ('${picture}', false) RETURNING pictureid`;
                const pictureResult = await pool.query(pictureInsertQuery);
                const pictureID = pictureResult.rows[0].pictureid;
        
                const linkPictureToAnswerQuery = `INSERT INTO AlertsAnswersPictures (AlertsAnswersID, pictureid) VALUES (${answerID}, ${pictureID})`;
                await pool.query(linkPictureToAnswerQuery);
            }
        
            return { message: 'Images ajoutées avec succès à la réponse' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de l\'ajout d\'images à la réponse');
        }
    }

    static async AlertsAnswersPicturesGet(answerID, pictureID) {
        try {
            const pictureQuery = `
                SELECT pictureID, image
                FROM AlertsAnswersPictures
                WHERE AlertsAnswersID = ${answerID} AND pictureID = ${pictureID}`;
            
            const result = await pool.query(pictureQuery);
        
            const picture = result.rows[0];
            if (!picture) {
                throw new Error('Image non trouvée');
            }
        
            return picture;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération de l\'image liée à la réponse');
        }
    }

    static async AlertsAnswersPicturesUpdate(answerID, pictureID, image) {
        try {
            const pictureUpdateQuery = `
                UPDATE AlertsAnswersPictures
                SET image = '${image}'
                WHERE AlertsAnswersID = ${answerID} AND pictureID = ${pictureID}`;
            
            await pool.query(pictureUpdateQuery);
        
            return { message: 'Image liée à la réponse mise à jour avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la mise à jour de l\'image liée à la réponse');
        }
    }

    static async AlertsAnswersPicturesDelete(answerID, pictureID) {
        try {
            const deletePictureLinkQuery = `
                DELETE FROM AlertsAnswersPictures
                WHERE AlertsAnswersID = ${answerID} AND pictureID = ${pictureID}`;
            
            await pool.query(deletePictureLinkQuery);
        
            const pictureUsageCountQuery = `
                SELECT COUNT(*) AS count
                FROM AlertsAnswersPictures
                WHERE pictureID = ${pictureID}`;
            
            const pictureUsageCountResult = await pool.query(pictureUsageCountQuery);
            const usageCount = pictureUsageCountResult.rows[0].count;
        
            if (usageCount === 0) {
                const deletePictureQuery = `
                DELETE FROM Pictures
                WHERE pictureID = ${pictureID}`;
                
                await pool.query(deletePictureQuery);
            }
        
            return { message: 'Image liée à la réponse supprimée avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la suppression de l\'image liée à la réponse');
        }
    }
      



}

module.exports = { AlertsAnswersPictures }