const pool = require('../db/db')

class AlertsAnswers {

    static async AlertsAnswersCreate(userID, content, title, linkedAnimalProof, pictures) {
        try {
            const answerInsertQuery = `
                INSERT INTO AlertsAnswers (userID, content, title, linkedAnimalProof) 
                VALUES (${userID}, '${content}', '${title}', ${linkedAnimalProof}) 
                RETURNING AlertsAnswersID`;
            
            const answerResult = await pool.query(answerInsertQuery);
            const answerID = answerResult.rows[0].AlertsAnswersID;
        
            for (const picture of pictures) {
                const pictureInsertQuery = `INSERT INTO Pictures (image, forUsers) VALUES ('${picture}', false) RETURNING pictureid`;
                const pictureResult = await pool.query(pictureInsertQuery);
                const pictureID = pictureResult.rows[0].pictureid;
        
                const linkPictureToAnswerQuery = `INSERT INTO AlertsAnswersPictures (AlertsAnswersID, pictureid) VALUES (${answerID}, ${pictureID})`;
                await pool.query(linkPictureToAnswerQuery);
            }
        
            return { message: 'Réponse ajoutée avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la création de la réponse');
        }
    }

    static async AlertsAnswersGet(answerID) {
        try {
            const answerQuery = `
                SELECT aa.AlertsAnswersID, aa.userID, aa.content, aa.title, aa.linkedAnimalProof,
                    ap.pictureID, ap.image
                FROM AlertsAnswers aa
                LEFT JOIN AlertsAnswersPictures ap ON aa.AlertsAnswersID = ap.AlertsAnswersID
                WHERE aa.AlertsAnswersID = ${answerID}`;
            
            const result = await pool.query(answerQuery);
            const answer = result.rows[0];
        
            if (!answer) {
                throw new Error('Réponse non trouvée');
            }
        
            const formattedAnswer = {
                AlertsAnswersID: answer.AlertsAnswersID,
                userID: answer.userID,
                content: answer.content,
                title: answer.title,
                linkedAnimalProof: answer.linkedAnimalProof,
                pictures: result.rows.map(row => ({ pictureID: row.pictureID, image: row.image })),
            };
        
            return formattedAnswer;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération de la réponse');
        }
    }

    static async AlertsAnswersUpdate(answerID, content, title, linkedAnimalProof, pictures) {
        try {
            const answerUpdateQuery = `
                UPDATE AlertsAnswers
                SET content = '${content}', title = '${title}', linkedAnimalProof = ${linkedAnimalProof}
                WHERE AlertsAnswersID = ${answerID}`;
            
            await pool.query(answerUpdateQuery);
        
            const deletePicturesQuery = `DELETE FROM AlertsAnswersPictures WHERE AlertsAnswersID = ${answerID}`;
            await pool.query(deletePicturesQuery);
        
            for (const picture of pictures) {
                const pictureInsertQuery = `INSERT INTO Pictures (image, forUsers) VALUES ('${picture}', false) RETURNING pictureid`;
                const pictureResult = await pool.query(pictureInsertQuery);
                const pictureID = pictureResult.rows[0].pictureid;
        
                const linkPictureToAnswerQuery = `INSERT INTO AlertsAnswersPictures (AlertsAnswersID, pictureid) VALUES (${answerID}, ${pictureID})`;
                await pool.query(linkPictureToAnswerQuery);
            }
        
            return { message: 'Réponse mise à jour avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la mise à jour de la réponse');
        }
    }

    static async AlertsAnswersDelete(answerID) {
        try {
            const deletePicturesQuery = `DELETE FROM AlertsAnswersPictures WHERE AlertsAnswersID = ${answerID}`;
            await pool.query(deletePicturesQuery);
        
            const deleteAnswerQuery = `DELETE FROM AlertsAnswers WHERE AlertsAnswersID = ${answerID}`;
            await pool.query(deleteAnswerQuery);
        
            return { message: 'Réponse supprimée avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la suppression de la réponse');
        }
    }
      
      
    static async AlertsAnswersGetByAlertID(alertID) {
        try {
            const answersQuery = `
                SELECT aa.AlertsAnswersID, aa.userID, aa.content, aa.title, aa.linkedAnimalProof,
                    ap.pictureID, ap.image
                FROM AlertsAnswers aa
                LEFT JOIN AlertsAnswersPictures ap ON aa.AlertsAnswersID = ap.AlertsAnswersID
                WHERE aa.linkedAlertID = ${alertID}`;
            
            const results = await pool.query(answersQuery);
            
            const formattedAnswers = results.rows.map(row => ({
                AlertsAnswersID: row.AlertsAnswersID,
                userID: row.userID,
                content: row.content,
                title: row.title,
                linkedAnimalProof: row.linkedAnimalProof,
                pictures: results.rows.map(row => ({ pictureID: row.pictureID, image: row.image })),
            }));
        
            return formattedAnswers;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération des réponses liées à l\'alerte');
        }
    }
}

module.exports = { AlertsAnswers }