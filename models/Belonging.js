const pool = require('../db/db')

class Belonging {

    static async BelongingGetAllByUser(userID) {
        try {
            const belongingsQuery = `
                SELECT userID, animalID, dateStatement
                FROM Belonging
                WHERE userID = ${userID}`;
            
            const results = await pool.query(belongingsQuery);
        
            const formattedBelongings = results.rows.map(row => ({
                userID: row.userID,
                animalID: row.animalID,
                dateStatement: row.dateStatement,
            }));
        
            return formattedBelongings;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération de toutes les relations de propriété pour l\'utilisateur');
        }
    }

    // Existe mais ne devrait pas être utilisé car un animal n'appartient qu'à un seul propriétaire
    static async BelongingGetAllByAnimal(animalID) {
        try {
            const belongingsQuery = `
                SELECT userID, animalID, dateStatement
                FROM Belonging
                WHERE animalID = ${animalID}`;
            
            const results = await pool.query(belongingsQuery);
        
            const formattedBelongings = results.rows.map(row => ({
                userID: row.userID,
                animalID: row.animalID,
                dateStatement: row.dateStatement,
            }));
        
            return formattedBelongings;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération de toutes les relations de propriété pour l\'animal');
        }
    }

    static async BelongingGet(userID, animalID) {
        try {
            const belongingQuery = `
                SELECT userID, animalID, dateStatement
                FROM Belonging
                WHERE userID = ${userID} AND animalID = ${animalID}`;
            
            const result = await pool.query(belongingQuery);
        
            const belonging = result.rows[0];
            if (!belonging) {
                throw new Error('Relation de propriété non trouvée');
            }
        
            return belonging;
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la récupération de la relation de propriété');
        }
    }

    static async BelongingCreate(userID, animalID, dateStatement) {
        try {
            const belongingInsertQuery = `
                INSERT INTO Belonging (userID, animalID, dateStatement)
                VALUES (${userID}, ${animalID}, '${dateStatement}')
                RETURNING userID, animalID`;
            
            const result = await pool.query(belongingInsertQuery);
            const createdBelonging = {
                userID: result.rows[0].userID,
                animalID: result.rows[0].animalID,
                dateStatement: dateStatement,
            };
        
            return { message: 'Relation de propriété créée avec succès', belonging: createdBelonging };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la création de la relation de propriété');
        }
    }   

    static async BelongingUpdate(userID, animalID, dateStatement) {
        try {
            const belongingUpdateQuery = `
                UPDATE Belonging
                SET dateStatement = '${dateStatement}'
                WHERE userID = ${userID} AND animalID = ${animalID}`;
            
            await pool.query(belongingUpdateQuery);
        
            return { message: 'Relation de propriété mise à jour avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la mise à jour de la relation de propriété');
        }
    }

    static async BelongingDelete(userID, animalID) {
        try {
            const deleteBelongingQuery = `
                DELETE FROM Belonging
                WHERE userID = ${userID} AND animalID = ${animalID}`;
            
            await pool.query(deleteBelongingQuery);
        
            return { message: 'Relation de propriété supprimée avec succès' };
        } catch (error) {
            console.error(error);
            throw new Error('Erreur lors de la suppression de la relation de propriété');
        }
    }

}

module.exports = { Belonging }