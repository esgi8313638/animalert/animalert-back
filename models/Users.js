const pool = require('../db/db')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


class User {
    static async getAll() {
        const result = await pool.query('SELECT * FROM Users');
        return result.rows;
    }

    static async getById(id) {
        const result = await pool.query(`SELECT * FROM Users WHERE userID = ${id}`);
        return result.rows[0];
    }

    static async getByEmail(email) {
        const result = await pool.query(`SELECT * FROM Users WHERE email = ${email}`);
        return result.rows[0];
    }

    static async getByPhone(phone) {
        const result = await pool.query(`SELECT * FROM Users WHERE telnumber = ${phone}`);
        return result.rows[0];
    }

    static async createUser(user) {
        const { email, password, telNumber, name, surname, firstName, address, localization, picture } = user;
        const result = await pool.query(
            `INSERT INTO Users (email, password, telNumber, name, surname, firstName, address, localization, picture) 
            VALUES (${email}, ${password}, ${telNumber}, ${name}, ${surname}, ${firstName}, ${address}, ${localization}, ${picture}) RETURNING *`
        );
        return result.rows[0];
    }

    static async updateUser(id, updatedFields) {
        const { email, password, telNumber, name, surname, firstName, address, localization, picture } = updatedFields;
        const result = await pool.query(
            `UPDATE Users SET email = ${email}, password = ${password}, telNumber = ${telNumber}, name = ${name}, surname = ${surname}, 
            firstName = ${firstName}, address = ${address}, localization = ${localization}, picture = ${picture} WHERE userID = ${id} RETURNING *`
        );
        return result.rows[0];
    }

    static async deleteUser(id) {
        const result = await pool.query(`DELETE FROM Users WHERE userID = ${id} RETURNING *`);
        return result.rows[0];
    }

    static async authenticate(email, password) {
        const user = await pool.query(`SELECT * FROM Users WHERE email = ${email}`);

        if (!user.rows[0]) {
            throw new Error('Utilisateur non trouvé');
        }

        const isPasswordValid = await bcrypt.compare(password, user.rows[0].password);

        if (!isPasswordValid) {
            throw new Error('Mot de passe incorrect');
        }

        const token = jwt.sign({ userID: user.rows[0].userID, email: user.rows[0].email }, JWT_SECRET, {
            expiresIn: '1h', // Durée de validité du token
        });

        return { user: user.rows[0], token };
    }

    static async verifyToken(token) {
        try {
            const decoded = jwt.verify(token, JWT_SECRET);
            return decoded;
        } catch (error) {
            throw new Error('Token invalide');
        }
    }

    static async logout(token) {
        User.blacklist = User.blacklist || [];
        User.blacklist.push(token);
    }
}

module.exports = { User };
  