const pool = require('./db/db');

class Picture {
	static async PictureGetAll() {
		try {
			const pictureQuery = `SELECT pictureID, image, forUsers FROM Pictures`;
			const result = await pool.query(pictureQuery);
		
			const picture = result.rows[0];
			if (!picture) {
				throw new Error('Images non trouvées');
			}
		
			return picture;
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la récupération des images');
		}
	}
  

	static async PictureGet(pictureID) {
		try {
			const pictureQuery = `SELECT pictureID, image, forUsers FROM Pictures WHERE pictureID = ${pictureID}`;
			const result = await pool.query(pictureQuery);
		
			const picture = result.rows[0];
			if (!picture) {
				throw new Error('Image non trouvée');
			}
		
			return picture;
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la récupération de l\'image');
		}
	}

	static async PictureCreate(image, forUsers) {
		try {
			const pictureInsertQuery = `INSERT INTO Pictures (image, forUsers) VALUES ('${image}', ${forUsers}) RETURNING pictureid`;
			const pictureResult = await pool.query(pictureInsertQuery);
			const pictureID = pictureResult.rows[0].pictureid;
		
			return { pictureID, message: 'Image ajoutée avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la création de l\'image');
		}
	}

	  static async PictureUpdate(pictureID, image, forUsers) {
		try {
			const pictureUpdateQuery = `
				UPDATE Pictures
				SET image = '${image}', forUsers = ${forUsers}
				WHERE pictureID = ${pictureID}`;
			
			await pool.query(pictureUpdateQuery);
		
			return { message: 'Image mise à jour avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la mise à jour de l\'image');
		}
	}

	  static async PictureDelete(pictureID) {
		try {
			const deletePictureQuery = `DELETE FROM Pictures WHERE pictureID = ${pictureID}`;
			await pool.query(deletePictureQuery);
		
			return { message: 'Image supprimée avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la suppression de l\'image');
		}
	}

}

module.exports = { Picture };
  