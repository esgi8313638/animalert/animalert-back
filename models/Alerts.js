const pool = require('../db/db')

class Alert {
	static async getAll() {
		const result = await pool.query('SELECT * FROM Alerts');
		return result.rows;
	}

	static async getById(id) {
		const result = await pool.query(`SELECT * FROM Alerts WHERE alertID = ${id}`);
		return result.rows[0];
	}

	static async getAlertLocation(id) {
		const result = await pool.query(`SELECT location FROM Alerts WHERE alertID = ${id}`);
		return result.rows[0];
	}

	static async getAlertAnimal(id) {
		const result = await pool.query('', [id]);
		
		return result.rows[0];
	}

	//pictures est un tableau de chaine texte BASE64
	static async AlertUserCreate(userid, location, animalid, pictures) {
		try {
			const alertInsertQuery = `INSERT INTO Alerts (userid, location, resolved, animalid) VALUES (${userid}, '${location}', 0, ${animalid}) RETURNING alertid`;
			const alertResult = await pool.query(alertInsertQuery);
			const alertid = alertResult.rows[0].alertid;
			
			for (const picture of pictures) {
				const pictureInsertQuery = `INSERT INTO Pictures (image, forUsers) VALUES ('${picture}', false) RETURNING pictureid`;
				const pictureResult = await pool.query(pictureInsertQuery);
				const pictureid = pictureResult.rows[0].pictureid;
		
				const linkPictureToAlertQuery = `INSERT INTO AlertsPictures (alertid, pictureid) VALUES (${alertid}, ${pictureid})`;
				await pool.query(linkPictureToAlertQuery);
			}

			return { alertid };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la création de l\'alerte');
		}
	}

	static async AlertUserDelete(alertid) {
		try {
			const deletePicturesQuery = `DELETE FROM AlertsPictures WHERE alertid = ${alertid}`;
			await pool.query(deletePicturesQuery);
		
			const deleteAlertQuery = `DELETE FROM Alerts WHERE alertid = ${alertid}`;
			await pool.query(deleteAlertQuery);
		
			return { message: 'Alerte supprimée avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la suppression de l\'alerte');
		}
	}

	static async AlertUserUpdate(alertid, location, resolved, pictures) {
		try {
			const updateAlertQuery = `UPDATE Alerts SET location = '${location}', resolved = ${resolved} WHERE alertid = ${alertid}`;
			await pool.query(updateAlertQuery);
		
			return { message: 'Alerte mise à jour avec succès' };
		} catch (error) {
			console.error(error);
			throw new Error('Erreur lors de la mise à jour de l\'alerte');
		}
	}

	
}

module.exports = { Alert };
  